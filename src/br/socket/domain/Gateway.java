package br.socket.domain;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Gateway extends Thread{
	
	private ServerSocket originSocket;
	private Socket connectionOriginSocket;
	private Socket connectionDestinySocket;
	private Integer gatewayPort = null;
	private Integer destinyPort = null;
	private String name = null;
	
	
	public Gateway(String name, Integer gatewayPort, Integer destinyPort) {
		this.name = name;
		this.gatewayPort = gatewayPort;
		this.destinyPort = destinyPort;
	}

	@Override
	public void run() {
		try {
//			System.out.println("Starting " +name);
			createConnectionWithOrigin();
			String receivedFile = receiveFile();
			createConnectionWithDestiny();
			fowardFileToDestiny(receivedFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}	
	
	private String receiveFile() throws IOException {
		String receivedFile = null;
		DataInputStream  dataInputStream = new DataInputStream(connectionOriginSocket.getInputStream());
		while (true) {
			if (dataInputStream.available() > 0) {
				receivedFile = dataInputStream.readLine() + "\n";
				break;
			} 
		}
		return receivedFile;
	}

	private void createConnectionWithOrigin() throws IOException {
		originSocket = new ServerSocket(gatewayPort);
		connectionOriginSocket = originSocket.accept();
	}

	private void createConnectionWithDestiny() throws IOException {
		connectionDestinySocket = new Socket("localhost", destinyPort);
	}
	
	private void fowardFileToDestiny(String fileContent) throws IOException{
		DataOutputStream dataOutputStream = new DataOutputStream(connectionDestinySocket.getOutputStream());
		long start = System.currentTimeMillis();
		dataOutputStream.writeBytes(fileContent);
		dataOutputStream.flush();
	    dataOutputStream.close();
	    System.out.println(System.currentTimeMillis() - start);
	}

	public Integer getPort() {
		return destinyPort;
	}

	public void setPort(Integer port) {
		this.destinyPort = port;
	}
	
}