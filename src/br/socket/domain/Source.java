package br.socket.domain;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class Source extends Thread {

	private Socket sourceSocket;
	private Integer targetPort;
	private String ip = "localhost";
	private String name;
	//public static String fileToSend = "origin.txt";
	public static String fileToSend = "origin2.txt";
	private Metrics metrics;

    public Source(String name, Integer targetPort, Metrics metrics) {
        super();
        this.targetPort = targetPort;
        this.name = name;
        this.metrics = metrics;
    }


	@Override
	public void run() {
		try {
			//System.out.println("Starting... " + name);
			createConnection();
			sendFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

    private void createConnection() throws IOException {
        sourceSocket = new Socket(ip, targetPort);
    }

    private void sendFile() throws IOException {
        BufferedReader arqBuffer;
        ArrayList<String> linhasArray = new ArrayList<>();
        String linha;

        arqBuffer = new BufferedReader(new FileReader((new File(".")).getCanonicalPath() + "/" + fileToSend));
        linha = arqBuffer.readLine() + "\n";
        linhasArray.add(linha);

        while (linha != null) {
            linha = arqBuffer.readLine();
            linhasArray.add(linha);
        }
        arqBuffer.close();

        linhasArray.remove(linhasArray.size() - 1);
        linha = linhasArray.get(0);

        for (int i = 1; i < linhasArray.size(); i++) {
            linha += linha = linhasArray.get(i) + "\n";
        }

        DataOutputStream dataOutputStream = null;
        for (int i = 0; i < metrics.getNumberOfRequests(); i++) {
            long currentTimeMillis = System.currentTimeMillis();

            try {
                Thread.sleep(metrics.getGenerationIntervalDelay());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            metrics.getServerAllocationTimes().add(System.currentTimeMillis());

            dataOutputStream = new DataOutputStream(sourceSocket.getOutputStream());
            dataOutputStream.writeBytes(currentTimeMillis + "#" + i + "-" + linha);
            dataOutputStream.flush();
        }

//		DataInputStream c = new DataInputStream(sourceSocket.getInputStream());
//	    System.err.println("The result from server was: \n" + readData());

        dataOutputStream.close();
    }
}
