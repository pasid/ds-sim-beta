package br.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Utils {


    public static void main(String[] args) {
        String[] a = divideString("dddddddd", 3);
        System.out.println();
    }

    public static String[] divideString(String str, int numberOfParts) {
        while (str.length() % numberOfParts != 0) {
            str += "a";
        }

        //n determines the variable that divide the string in 'n' equal parts
        int n = numberOfParts;
        int temp = 0, chars = str.length() / n;
        //Stores the array of string
        String[] equalStr = new String[n];
        //Check whether a string can be divided into n equal parts
        for (int i = 0; i < str.length(); i = i + chars) {
            //Dividing string in n equal part using substring()
            String part = str.substring(i, i + chars);
            equalStr[temp] = part;
            temp++;
        }

        return equalStr;
    }

    /**
     * This method counts how many times each individual word appears in the text.
     *
     * @param text
     * @return
     */
    public static String wordCount(String text) {
        Set<String> set = null;
        Map<String, Integer> mapaPalavras = new HashMap<String, Integer>();
        StringBuffer result = new StringBuffer();
        text = text.replace("\n", " ");
        List<String> asList = Arrays.asList(text.split(" "));

        set = new LinkedHashSet<String>();
        set.addAll(asList);

        for (String word : set) {
            mapaPalavras.put(word, 0);
        }

        for (String word : asList) {
            mapaPalavras.put(word, mapaPalavras.get(word) + 1);
        }

        for (String word : mapaPalavras.keySet()) {
            result.append(word + " " + mapaPalavras.get(word) + "\n");
        }

        return result.toString();
    }

    public long[] convertMiliseconds(long moment){
        long milisecond = moment%1000;
        long second = moment / 1000;
        long minute = second / 60;
        second = second % 60;
        long hour = minute / 60;
        minute = minute % 60;

        long[] time= {hour, minute, second, milisecond};
        return time;
    }
}
