package main;

import br.socket.domain.*;

import java.util.ArrayList;
import java.util.List;

public class SimulationGroup2 {

	public static void main(String[] args) throws InterruptedException {

		int numberOfRequests = 40;
		int port = 3000;
		int generationIntevalDelay = 50; // taxa de geração = 1/50 = 0.02
//		int generationIntevalDelay = 33; // taxa de geração = 1/33 = 0.03;
//		int generationIntevalDelay = 25; // taxa de geração = 1/25 = 0.04;
//		int generationIntevalDelay = 20; // taxa de geração = 1/20 = 0.05;
//		int generationIntevalDelay = 16; // taxa de geração = 1/16 = 0.06;

		// taxa de gereção = 1/generationIntevalDelay => lambda
		double taxa = 1.0d/generationIntevalDelay;

		Metrics metrics = new Metrics();
		metrics.setGenerationIntervalDelay(generationIntevalDelay);
		metrics.setNumberOfRequests(numberOfRequests);
		
		metrics.setRequestsInicialTime(new Long[numberOfRequests]);
		metrics.setRequestsFinalTime(new Long[numberOfRequests]);
		
		metrics.setMaxSizeQueue(3);

		List<Server> servers = new ArrayList<Server>();
		servers.add(new Server("server01", 1, TypeServer.COMPUTER, metrics));
		servers.add(new Server("server02", 1, TypeServer.COMPUTER, metrics));
		servers.add(new Server("server03", 1, TypeServer.COMPUTER, metrics));
		servers.add(new Server("server04", 1, TypeServer.SMARTPHONE, metrics));
		servers.add(new Server("server05", 1, TypeServer.SMARTPHONE, metrics));

		Cluster cluster = new Cluster("cluster1", port,  metrics, true);
		cluster.setServers(servers);
		cluster.start();

		Source source1 = new Source("source1", port, metrics);
		source1.start();
		
	}
}
