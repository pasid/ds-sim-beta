package main;

import br.socket.domain.*;

import java.util.ArrayList;
import java.util.List;

public class SimulationGroup4 {

	public static void main(String[] args) throws InterruptedException {

		int numberOfRequests = 30;
		int qtdCores = 1;

		int generationIntevalDelay = 50; // taxa de geração = 1/50 = 0.02
//		int generationIntevalDelay = 33; // taxa de geração = 1/33 = 0.03;
//		int generationIntevalDelay = 25; // taxa de geração = 1/25 = 0.04;
//		int generationIntevalDelay = 20; // taxa de geração = 1/20 = 0.05;
//		int generationIntevalDelay = 16; // taxa de geração = 1/16 = 0.06;

		// taxa de gereção = 1/generationIntevalDelay => lambda
		double taxa = 1.0d/generationIntevalDelay;

		Metrics metrics = new Metrics();
		metrics.setGenerationIntervalDelay(generationIntevalDelay);
		metrics.setNumberOfRequests(numberOfRequests);
		//
		
		metrics.setRequestsInicialTime(new Long[numberOfRequests]);
		metrics.setRequestsFinalTime(new Long[numberOfRequests]);
		
		metrics.setMaxSizeQueue(3);
		//
		Cluster cluster = null;
		cluster = new Cluster("cluster1", 3000, metrics, true);

		Source source1 = new Source("source1", 3000, metrics);

		List<Server> servers = new ArrayList<Server>();
		servers.add(new Server("cloud-1", qtdCores, TypeServer.COMPUTER, metrics)); // cloud1
		servers.add(new Server("cloud-2", qtdCores, TypeServer.COMPUTER, metrics)); // cloud2
		// servers.add(new Server("server03", 1, metrics));
		// servers.add(new Server("server04", 1, metrics));
		cluster.setServers(servers);

		System.out.print(generationIntevalDelay + "\t");


		//for (int i = 0; i <= 80; i = i + 10) {
		cluster.start();
		source1.start();

//			try {
//				Thread.sleep(2000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}

		//System.out.println("req"+cluster.getMetrics().getRequests());
		//System.out.println("nR"+cluster.getMetrics().getNumberOfRequests());


		//if (cluster.getMetrics().getRequests() >= cluster.getMetrics().getNumberOfRequests()) {
		//System.out.println(cluster.getMetrics().getMRT().toString() + ":");
		//System.out.println(cluster.getMetrics().getDropedRequestsPercent());

		//System.exit(0);
		//}
		//}

	}
}
