package main;

import java.util.ArrayList;
import java.util.List;

import br.socket.domain.*;
import br.socket.domain.TypeServer;

public class SimulationStarter {

	public static void main(String[] args) throws InterruptedException {

		int numberOfRequests = 20;
		int port = 3000;

		int generationIntevalDelay = 1000; // taxa de geração = 1/50 = 0.02
//		int generationIntevalDelay = 33; // taxa de geração = 1/33 = 0.03;
//		int generationIntevalDelay = 25; // taxa de geração = 1/25 = 0.04;
//		int generationIntevalDelay = 20; // taxa de geração = 1/20 = 0.05;
//		int generationIntevalDelay = 16; // taxa de geração = 1/16 = 0.06;

		// taxa de gereção = 1/generationIntevalDelay => lambda
		double taxa = 1.0d/generationIntevalDelay;

		Metrics metrics = new Metrics();
		metrics.setGenerationIntervalDelay(generationIntevalDelay);
		metrics.setNumberOfRequests(numberOfRequests);
		
		//
		
		metrics.setRequestsInicialTime(new Long[numberOfRequests]);
		metrics.setRequestsFinalTime(new Long[numberOfRequests]);
		
		//
		//
		metrics.setMaxSizeQueue(3);
		//

		List<Server> servers = new ArrayList<Server>();
		servers.add(new Server("server01", 1, TypeServer.COMPUTER, metrics));
		servers.add(new Server("server02", 1, TypeServer.COMPUTER, metrics));
		servers.add(new Server("server03", 1, TypeServer.SMARTPHONE, metrics));
		servers.add(new Server("server04", 1, TypeServer.SMARTPHONE, metrics));

		Cluster cluster = new Cluster("cluster1", port,  metrics, true);
		cluster.setServers(servers);
		cluster.start();

		Source source1 = new Source("source1", port, metrics);
		source1.start();

	}
}

/*

package main;

        import java.util.ArrayList;
        import java.util.List;

        import br.socket.domain.Cluster;
        import br.socket.domain.Metrics;
        import br.socket.domain.Server;
        import br.socket.domain.Source;

public class SimulationStarter {

    public static void main(String[] args) throws InterruptedException {

        int numberOfRequests = 30;
        int generationIntevalDelay = 16;
        int qtdCores = 1;



        Metrics metrics = new Metrics();
        metrics.setGenerationIntervalDelay(generationIntevalDelay);
        metrics.setNumberOfRequests(numberOfRequests);
        //
        metrics.setMaxSizeQueue(3);
        //
        Cluster cluster = null;
        cluster = new Cluster("cluster1", 3000, metrics);

        Source source1 = new Source("source1", 3000, metrics);

        List<Server> servers = new ArrayList<Server>();
        servers.add(new Server("cloud-1", qtdCores, metrics)); // cloud1
        servers.add(new Server("cloud-2", qtdCores, metrics)); // cloud2
        // servers.add(new Server("server03", 1, metrics));
        // servers.add(new Server("server04", 1, metrics));
        cluster.setServers(servers);

        System.out.print(generationIntevalDelay + "\t");


        //for (int i = 0; i <= 80; i = i + 10) {
        cluster.start();
        source1.start();

//			try {
//				Thread.sleep(2000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}

        //System.out.println("req"+cluster.getMetrics().getRequests());
        //System.out.println("nR"+cluster.getMetrics().getNumberOfRequests());


        //if (cluster.getMetrics().getRequests() >= cluster.getMetrics().getNumberOfRequests()) {
        //System.out.println(cluster.getMetrics().getMRT().toString() + ":");
        //System.out.println(cluster.getMetrics().getDropedRequestsPercent());

        //System.exit(0);
        //}
        //}

    }
}



package main;

import java.util.ArrayList;
import java.util.List;

import br.socket.domain.Cluster;
import br.socket.domain.TypeServer;
import br.socket.domain.Metrics;
import br.socket.domain.Server;
import br.socket.domain.Source;

public class SimulationStarter {

	public static void main(String[] args) throws InterruptedException {

		List<Integer> numberOfRequestsArr = new ArrayList<Integer>();

		for (int i = 2; i <= 20; i+=2) {
			numberOfRequestsArr.add(i);
		}

		int numberOfRequests = 0;
		int generationIntevalDelay = 10;
		int port = 3000;

		//for (int i = 0; i < numberOfRequestsArr.size(); i++) {
			//System.out.println("Req: " + numberOfRequestsArr.get(i));
			numberOfRequests = 2;

			Metrics metrics = new Metrics();
			metrics.setGenerationIntervalDelay(generationIntevalDelay);
			metrics.setNumberOfRequests(numberOfRequests);
			//
			metrics.setMaxSizeQueue(3);
			//

			List<Server> servers = new ArrayList<Server>();

			servers.add(new Server("server01", 8, TypeServer.COMPUTER, metrics));
			servers.add(new Server("server02", 2, TypeServer.COMPUTER, metrics));
			//servers.add(new Server("server03", 2, TypeServer.COMPUTER, metrics));
			//servers.add(new Server("server04", 2, TypeServer.COMPUTER, metrics));
			//servers.add(new Server("server05", 1, TypeServer.SMARTPHONE, metrics));

			Cluster cluster = new Cluster("cluster1", port,  metrics);
			cluster.setServers(servers);
			cluster.start();

			Source source1 = new Source("source1", port, metrics);
			source1.start();
		//}
		/*
		 * Objetivo:
		 * 1 - Tempo Médio em Fila
		 * */
/*
	}
            }

*/
